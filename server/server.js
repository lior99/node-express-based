const express = require('express');
const items = require('./items');

const app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

app.get('/', (request, response) => {
    response.send('Web server is running');
});

app.get('/world', (req, res) => {
    const result = [
        { id : 1, type : "file", name : "todo.mvc"},
        { id : 2, type : "file", name : "readme.txt"},
        { id : 3, type : "file", name : "log.log"},
        { id : 4, type : "folder", name : "notes"},
        { id : 5, type : "folder", name : "documents"},
    ]

    res.send({ status:200, result});
})


app.get('/items', (request, res) => {   
    res.json(items);
})

// main listen point
app.listen(999, () => {
    console.log('app is now listening on port 999');

});