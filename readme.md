# README

### Description
using node.js, express and react to build something.... :)


### Installation
1. run yarn install


### Running
1. run the server by "yarn server" which will run
"nodemon server/server.js --watchnodemon" :

2. Data is available on http://localhost:999/items

3. run yarn start to start the webpack dev sever

4. browse to http://localhost:44/
 