import React, { Component } from 'react';
import logo from './logo.svg';
//import './App.css';
import './style.css'
import ItemsTable from './components/ItemsTable';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

class App extends Component {
  render() {
    return (
        <MuiThemeProvider>
            <div className="App">       
                <ItemsTable />
            </div>
        </MuiThemeProvider>
      
    );
  }
}

export default App;
