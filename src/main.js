window.addEventListener('DOMContentLoaded', init);

async function init() {
    try {
        const response = await fetch('http://localhost:999/items');
        const items = await response.json();
        writeContentsToScreen(items);
    }
    catch(err) {
        console.group('error fetching data from server');
        console.error('err', err);
        console.groupEnd();
    }
} 


const writeContentsToScreen = (data) => {
    const rows = data.map(item => {
        return `<tr>
                    <td>${item.itemId}</td>
                    <td>${item.type}</td>
                </tr>`
    }).join('');

    const table = `<table class="items-table">
                        <thead>
                            <tr>
                                <th class="header">Item Id</th>
                                <th class="header">Type</th>
                            </tr>
                        </thead>
                        <tbody>
                        ${rows}
                        </tbody>
                    </table>`;

    document.querySelector('#items').innerHTML = table;
}