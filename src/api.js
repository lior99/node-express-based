const api = {
    getItems : async () => {
        try {
            const url = 'http://localhost:999/items';
            const response = await fetch(url);
            const items = await response.json();
            return items;
        }
        catch(err) {
            throw err;
        }
    }
}

export default api;