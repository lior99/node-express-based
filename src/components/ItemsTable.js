import React from 'react';
import api from '../api';
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
  } from 'material-ui/Table';
  

class ItemsTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            items : null
        }
    }

    async componentDidMount() {
        const items = await api.getItems();
        this.setState({ items });
    }

    render() {
        const items = this.state.items;
        if (items === null) {
            return <div>no items</div>
        }

        return (
            <div>
                <Table>
                    <TableHeader>
                        <TableRow>
                            <TableHeaderColumn>Id</TableHeaderColumn>
                            <TableHeaderColumn>Type</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody>
                        { 
                            items.map((item, index) => {
                                const key = `key_${index}`;
                                return  (
                                        <TableRow>
                                            <TableRowColumn>{ item.itemId }</TableRowColumn>
                                            <TableRowColumn>{ item.type }</TableRowColumn>  
                                        </TableRow>
                                )
                            })
                        }
                    </TableBody>
                    
                    
                </Table>
            </div>
            
        )
    }
}

export default ItemsTable;